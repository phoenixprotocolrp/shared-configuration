# Copyright Phoenix Protocol - All Rights Reserved
#   Unauthorized copying of this file, via any medium is strictly prohibited
#   Proprietary and confidential
# @author sircapsalot
# @since Oct 4, 2023

# frozen_string_literal: true

file_types = %w[.lua .js]
no_cs = []

files = file_types.each_with_object([]) do |file_type, files|
  Dir[File.join('src', '**', "*#{file_type}")].each { |f| files << f }
end.each do |file|
  File.open(file, 'r') do |f|
    next if f.readline =~ /Copyright/i

    no_cs << file
  end
end

abort "No copyright in \n\t#{no_cs.join("\n\t")}" unless no_cs.empty?
